package musicbox.com.musicbox.bean;


public class VideoItem {

    private int id;
    private String videoId;
    private String title;
    private String owner;
    private String thumbnailUrl;

    public VideoItem() {

    }

    public VideoItem(int id, String videoId, String title, String owner, String thumbnailUrl) {
        this.id = id;
        this.videoId = videoId;
        this.title = title;
        this.owner = owner;
        this.thumbnailUrl = thumbnailUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVideoId() { return videoId; }

    public void setVideoId(String videoId) { this.videoId = videoId; }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getThumbnailUrl() { return thumbnailUrl; }

    public void setThumbnailUrl(String thumbnailUrl) { this.thumbnailUrl = thumbnailUrl; }

}
