package musicbox.com.musicbox.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import musicbox.com.musicbox.R;
import musicbox.com.musicbox.VideoDetailActivity;
import musicbox.com.musicbox.bean.VideoItem;

public class VideoItemAdapter extends ArrayAdapter<VideoItem> {

    private Context context;
    private VideoItem videoItem;

    public VideoItemAdapter(Context context, ArrayList<VideoItem> videos) {
        super(context, 0, videos);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        videoItem = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.video_list_item, parent, false);
        }
        // Lookup view for data population
        TextView videoTitle = (TextView) convertView.findViewById(R.id.video_title);
        TextView videoOwner = (TextView) convertView.findViewById(R.id.video_owner);

        videoOwner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, VideoDetailActivity.class);
                intent.putExtra("PARAM", videoItem.getVideoId());
                context.startActivity(intent);
            }
        });


        ImageView imageView = (ImageView) convertView.findViewById(R.id.video_thumbnail);
        // Populate the data into the template view using the data object
        videoTitle.setText(videoItem.getTitle());
        videoOwner.setText(videoItem.getOwner());

        new DownloadImageTask(imageView).execute(videoItem.getThumbnailUrl());

        // Return the completed view to render on screen
        return convertView;
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}